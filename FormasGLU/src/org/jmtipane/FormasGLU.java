package org.jmtipane;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;



/**
 * Taller 3_Figuras GLU y GLUT
 * Programa que crea figuras de la librer�a GLU y GLUT
 * Rota a las figuras
 * @author Marcelo Tip�n
 * 18/07/202
 */
public class FormasGLU implements GLEventListener {
    
    //variables de Opengl
    static GL gl;
    static GLU glu;
    static GLUT glut;
    static float ang=0;
    
    /**
     * Crea una ventana y la muestra
     * @param args 
     */
    public static void main(String[] args) {
        Frame frame = new Frame("Formas GLU y GLUT");
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new FormasGLU());
        frame.add(canvas);
        frame.setSize(1000, 1000);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
    }

    /**
     * Inicualiza variables y color
     * @param drawable 
     */
    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
    }

    /**
     * Define la forma, perspectiva y dimensiones de la pantalla
     * @param drawable
     * @param x
     * @param y
     * @param width
     * @param height 
     */
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
//        glu.gluPerspective(45.0f, h, 1.0, 20.0);
        gl.glOrtho(-225, 225, 300, -300, -200, 200);
        
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    /**
     * Hace uso de variables GLU, GLUT y GL para definir formas y figuras
     * Hace uso de funciones gl para rotar y trasladar a la figura
     * @param drawable 
     */
    public void display(GLAutoDrawable drawable) {
        gl = drawable.getGL();
        glu = new GLU();
        glut = new GLUT();
        
        ang=ang+5;

        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();

        GLUquadric quad;
        quad = glu.gluNewQuadric();
        glu.gluQuadricDrawStyle(quad, GLU.GLU_FILL);
        
        gl.glTranslated(150, 60, 1);
        //Dibujar cinlindro
        gl.glPushMatrix();
        gl.glColor3f(1, 0, 0);
        gl.glTranslatef(-300, -300, 0);
        gl.glRotated(ang, 1, 1, 1);
        glu.gluCylinder(quad, 10, 10, 30, 10, 10);
        gl.glPopMatrix();
        
        //Dibuja un disco
        gl.glPushMatrix();
        gl.glColor3f(0, 1, 0);
        gl.glTranslatef(-290, -230, 0);
        gl.glRotated(ang, 1, 1, 1);
        glu.gluDisk(quad, 10, 30, 60, 50);
        gl.glPopMatrix();
        
        //Dibuja un disco parcial
        gl.glPushMatrix();
        gl.glColor3f(0, 0, 1);
        gl.glTranslatef(-300, -170, 0);
        gl.glRotated(ang, 1, 1, 1);
        glu.gluPartialDisk(quad, 10, 30, 60, 50, 10, 20);
        gl.glPopMatrix();
        
        //Dibuja una esfera
        gl.glPushMatrix();
        gl.glColor3f(0, 1, 1);
        gl.glTranslatef(-300, -100, 0);
        gl.glRotated(ang, 1, 1, 1);
        glu.gluSphere(quad, 20, 50, 20);
        gl.glPopMatrix();
        
        //Dibuja un cono solido
        gl.glPushMatrix();
        gl.glColor3f(1, 1, 0);
        gl.glTranslatef(-300, -40, 0);
        gl.glRotated(ang, 1, 1, 1);
        glut.glutSolidCone(20, 50, 50, 30);
        gl.glPopMatrix();

        //Dibuja un cubo solido
        gl.glPushMatrix();
        gl.glColor3f(1, 0, 1);
        gl.glTranslatef(-300, 50, 0);
        gl.glRotated(ang, 1, 1, 1);
        glut.glutSolidCube(30);
        gl.glPopMatrix();
        
        //Dibuja un cilindro solido
        gl.glPushMatrix();
        gl.glColor3f(1, 1, 1);
        gl.glTranslatef(-300, 120, 0);
        gl.glRotated(ang, 1, 1, 1);
        glut.glutSolidCylinder(10,50,50,60);
        gl.glPopMatrix();
        
        //Dibuja un dodecaedro solido
        gl.glPushMatrix();
        gl.glColor3f(1, 0, 0);
        gl.glTranslatef(-200, -300, 0);
        gl.glRotated(ang, 1, 1, 1);
        gl.glScaled(10, 10, 10);
        glut.glutSolidDodecahedron();
        gl.glPopMatrix();
        
        //Dibuja un icosaedro solido
        gl.glPushMatrix();
        gl.glColor3f(0, 1, 0);
        gl.glTranslatef(-200, -230, 0);
        gl.glRotated(ang, 1, 1, 1);
        gl.glScaled(20, 20, 20);
        glut.glutSolidIcosahedron();
        gl.glPopMatrix();
        
        //Dibuja un octaedro solido
        gl.glPushMatrix();
        gl.glColor3f(0, 0, 1);
        gl.glTranslatef(-200, -160, 0);
        gl.glRotated(ang, 1, 1, 1);
        gl.glScaled(20, 20, 20);
        glut.glutSolidOctahedron();
        gl.glPopMatrix();
        
        //Dibuja un rombo dodecaedro solido
        gl.glPushMatrix();
        gl.glColor3f(0, 1, 1);
        gl.glTranslatef(-200, -100, 0);
        gl.glRotated(ang, 1, 1, 1);
        gl.glScaled(20, 20, 20);
        glut.glutSolidRhombicDodecahedron();
        gl.glPopMatrix();
        
        //Dibuja una esfera solido
        gl.glPushMatrix();
        gl.glColor3f(1, 1, 0);
        gl.glTranslatef(-200, -40, 0);
        gl.glRotated(ang, 1, 1, 1);
        gl.glScaled(20, 20, 20);
        glut.glutSolidSphere(1, 20, 35);
        gl.glPopMatrix();
        
        //Dibuja una tetera solido
        gl.glPushMatrix();
        gl.glColor3f(1, 0, 1);
        gl.glTranslatef(-200, 50, 0);
        gl.glRotated(ang, 0, 0, 1);
        glut.glutSolidTeapot(20);
        gl.glPopMatrix();
        
        //Dibuja un toroide solido
        gl.glPushMatrix();
        gl.glColor3f(1, 1, 1);
        gl.glTranslatef(-200, 120, 0);
        gl.glRotated(ang, 1, 1, 0);
        glut.glutSolidTorus(10, 25, 50, 50);
        gl.glPopMatrix();
        
        //Dibuja un tetraedro solido
        gl.glPushMatrix();
        gl.glColor3f(1, 0, 0);
        gl.glTranslatef(-100, -300, 0);
        gl.glRotated(ang, 1, 1, 1);
        gl.glScaled(20, 20, 20);
        glut.glutSolidTetrahedron();
        gl.glPopMatrix();
        
        //Dibuja las lineas de un dodecaedro
        gl.glPushMatrix();
        gl.glColor3f(0, 1, 0);
        gl.glTranslatef(-100, -230, 0);
        gl.glRotated(ang, 1, 1, 1);
        gl.glScaled(10, 10, 10);
        glut.glutWireDodecahedron();
        gl.glPopMatrix();
        
        //Dibuja las lineas de un icosaedro
        gl.glPushMatrix();
        gl.glColor3f(0, 0, 1);
        gl.glTranslatef(-100, -160, 0);
        gl.glRotated(ang, 1, 1, 1);
        gl.glScaled(20, 20, 20);
        glut.glutWireIcosahedron();
        gl.glPopMatrix();
        
        //Dibuja las lineas de un octaedro
        gl.glPushMatrix();
        gl.glColor3f(0, 1, 1);
        gl.glTranslatef(-100, -100, 0);
        gl.glRotated(ang, 1, 1, 1);
        gl.glScaled(20, 20, 20);
        glut.glutWireOctahedron();
        gl.glPopMatrix();
        
        //Dibuja las lineas de un rombo dodecaedro
        gl.glPushMatrix();
        gl.glColor3f(1, 1, 0);
        gl.glTranslatef(-100, -40, 0);
        gl.glRotated(ang, 1, 1, 1);
        gl.glScaled(20, 20, 20);
        glut.glutWireRhombicDodecahedron();
        gl.glPopMatrix();
        
        //Dibuja las lineas de una esfera
        gl.glPushMatrix();
        gl.glColor3f(1, 0, 1);
        gl.glTranslatef(-100, 50, 0);
        gl.glRotated(ang, 1, 1, 1);
        glut.glutWireSphere(20, 20, 5);
        gl.glPopMatrix();
        
        //Dibuja las lineas de una tetera
        gl.glPushMatrix();
        gl.glColor3f(1, 1, 1);
        gl.glTranslatef(-100, 130, 0);
        gl.glRotated(ang, 1, 1, 1);
        glut.glutWireTeapot(30);
        gl.glPopMatrix();
        
        //Dibuja las lineas de un tetraedro
        gl.glPushMatrix();
        gl.glColor3f(1, 0, 0);
        gl.glTranslatef(0, -300, 0);
        gl.glRotated(ang, 1, 1, 1);
        gl.glScaled(20, 20, 20);
        glut.glutWireTetrahedron();
        gl.glPopMatrix();
        
        //Dibuja las lineas de un toroide
        gl.glPushMatrix();
        gl.glColor3f(0, 1, 0);
        gl.glTranslatef(0, -230, 0);
        gl.glRotated(ang, 1, 1, 1);
        glut.glutWireTorus(10, 25, 15, 15);
        gl.glPopMatrix();
        
        //Dibuja las lineas de un cono
        gl.glPushMatrix();
        gl.glColor3f(0, 0, 1);
        gl.glTranslatef(0, -150, 0);
        gl.glRotated(ang, 1, 1, 1);
        glut.glutWireCone(20, 50, 20, 20);
        gl.glPopMatrix();
        
        //Dibuja las lineas de un cubo
        gl.glPushMatrix();
        gl.glColor3f(0, 1, 1);
        gl.glTranslatef(0, -60, 0);
        gl.glRotated(ang, 1, 1, 1);
        glut.glutWireCube(20);
        gl.glPopMatrix();
        
        //Dibuja las lineas de un cubo
        gl.glPushMatrix();
        gl.glColor3f(1, 0, 1);
        gl.glTranslatef(0, 30, 0);
        gl.glRotated(ang, 1, 1, 1);
        glut.glutWireCylinder(10, 30, 10, 10);
        gl.glPopMatrix();
        
//        glut.glutWireCone(10, 15, 15, 20);
        // Flush all drawing operations to the graphics card
        gl.glFlush();
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }
}

